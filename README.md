# inria2ges1p5

Python script to convert Inria missions to a format compatible with GES 1point5

# Instructions

1. Télécharger les sources avec git ou l'archive
2. Télécharger et décompresser le fichier [cities500.zip](https://download.geonames.org/export/dump/cities500.zip) de [geonames.org](https://geonames.org) dans le dossier contentant le script
3. Exporter l'export ORELI au format .csv (avec une `,` comme délimiteur)
4. Lancer le script en adaptant l'exemple suivant :
```
$ python oreli_to_l1p5.py -y 2019 -i mon_listing_oreli.csv -o mon_listing_l1p5.tsv
```
5. Téléverser `mon_listing_l1p5.tsv` dans l'outil GES 1point5 !

# Ce que fait le script

* Extraction des colonnes pertinentes
* Génération des colonnes `Mode` et `Aller Retour`
* Recherche des code pays à partie des codes aéroport pour les trajets en avions, et du nom des villes pour les trajets en train.